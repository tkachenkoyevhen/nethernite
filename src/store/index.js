import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    versions: [],
    packages: null
  },
  mutations: {
    SAVE_VERSIONS: (state, payload) => {
      state.versions = [...payload]
    },
    SAVE_PACKAGES: (state, payload) => {
      Vue.set(state, 'packages', payload)
    }
  },
  actions: {
    async versions({commit}) {
      const versions = await Vue.$http.get('/package/npm/jquery')
      if(versions?.data && versions?.status === 200) {
        commit('SAVE_VERSIONS', versions.data.versions)
      }
    },
    async search(_, param) {
      const search = await Vue.$http.get(`/search?text=${param}`)
      console.log(search, param)
    },
    async packages({ commit }, params) {
      const { version } = params
      const packages = await Vue.$http.get(`/package/npm/${version}`)
      if(packages?.data && packages.status === 200) {
        commit('SAVE_PACKAGES', packages.data)
      }
    }
  },
  modules: {
  }
})
