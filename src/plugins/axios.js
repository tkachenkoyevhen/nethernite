import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

const axiosInstance = axios.create({
  baseURL: 'https://data.jsdelivr.com/v1/',
  headers: {}
})

Vue.use(VueAxios, axiosInstance)

export default axiosInstance